package com.pritamsahana.GettingStarted;

// Types of comments -

/**
 * A documentation comment(ignored by the compiler but useful to other programmers, compiler ignores everything
 * in between, used for automatically generated documentation)
 * The HelloWorldApp class implements an application that
 * simply prints "Hello World!" to standard output.
 */

// A single line comment(compiler ignores everything from // to the end of line)

/* Multiline comments */

public class HelloWorldApp {
    public static void main(String[] args) { // modifiers - public, static
        // public static(convention) or static public
        // args - argument(args or argv)
        // main - a method, it's the entry point for your application and will subsequently invoke all the other methods
        // required by our program. The main method accepts a single argument: an array of type String.
        System.out.println("Hello World"); // Display the string.
        // uses the System class from the core library to print the "Hello World!" message to standard output.
    }
}
